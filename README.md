# StudyGroup

The Python Club has closed.

However, interested users can join the Madison group interested in *"Computational Biology, Ecology, & Evolution"* (ComBEE):

> [**ComBEE**](https://combee-uw-madison.github.io/studyGroup/) is a group of researchers at UW-Madison interested in computational biology in ecology and evolution.  
> (See Calendar within for sessions dates and time.)

## 2023 

Python Club Intermediate/Advanced section study StudyGroup

This repository will host summary and/or session code.

See also https://bcrf.biochem.wisc.edu/python-club/

## Spring 2023

The material for Spring 2023 is:

* Book: Python for the Life Sciences – A Gentle Introduction. [Download instructions](https://bcrf.biochem.wisc.edu/2022/11/15/python-for-the-life-sciences-a-gentle-introduction/)
* The python code is publicly available at https://github.com/amberbiology/py4lifesci

## Reference

This web site is useful: https://www.w3schools.com/python/default.asp
