# -*- coding: utf-8 -*-

import re

def open_fasta(file_name):
    """Open Fasta sequence files.


    Parameters
    ----------
    file_name : Str
        File name.

    Returns
    -------
    fasta_seq : Str
        Seqeunce data contained in the Fasta file.

    """

    # open file
    seq_file = open(file_name, 'r')

    data = seq_file.read()

    seq_file.close()

    #index of first \n character
    index = data.find('\n')

    # get nucleotide sequence after first \n
    fasta_seq = data[index+1:].replace('\n', '')

    fasta_seq = fasta_seq.upper()

    return fasta_seq




def find_sequence(expression, sequence):
    """Find regular expression in sequence.

    Parameters
    ----------
    expression : Str
        Regular expression.
    sequence : Str
        String where the search is performed.

    Returns
    -------
    None.

    """

    hit_list = re.finditer(expression, sequence)

    print('{:<10s} {:<10s} {:s}'. format('start', 'end', 'match'))

    for hit in hit_list:

        match = hit.group(0) #group is a procedure to access information on the object

        start, end = hit.span() #span is the same as hit.start, hit.end
        
        c = match.count('A')
    
        if c == 1 and not 'GT'*11+'G' in match: #forces sequences to contain only one A, and limits less than 11 GT repeats

            print('{:<10d} {:<10d} {:s}'. format(start+1, end, match))
            
        #{:<10d} means 10 spaces in a row, < is justification to the left
        #{:s} is just for string






#Y =open_fasta('chrY.fna')

s = 'GTGAGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGTGA'

find_sequence('(GT){11,}GT?', s)

result = re.finditer('GA', s)
for r in result:
    print(r) #to print contents and see whats in the re.finditer result

'''
session notes

use [A-T] to look for letters in the alphabet between positions

to look for example patterns (like CATG or CGCG):
    'C(AT)?(GC)?G' where ? looks for one or 0 but would need to force length
    'C((AT)|(GT))G' where | functions as an or
    
    
use {} to look for repitions; to look for repeats of GT:
    '(GT)?' --> looks for 0 or 1
    '(GT){1,3}'--> looks for between 1-3 repetitions
    '(GT)+'--> looks for 1 or more repetitions
    '(GT)*'---> looks for 0 or more repetitions
    
doesn't match in a sliding scale, once it finds something it doesn't consider it for the next match

[AT] only looks for one item, A or T
(AT) looks for the whole block, AT


applications:
    looking for at least 12 GT repeats with optional extra G
    something that looks for at least 12 Gs
        find_sequence '(GT){11,}GT?'
    
    next question: what if one of the T's can be changed to an A? means that there could be less than 12 total GT
        '(GT)*(GA)(GT)*'--> looks for any repeats of GT followed by GA followed by any GT repeat. but issue is that you could get just GA here
        '(G[TA]){11,}T' --> issue here is that it can find GAGAGAGA...
            could do additional filtering and look for items that have only one A

        '(G[TA]){11,}G'--> still finds GAGAGAG....but limits length to at least 11 Gs
            add different parts to function to restrict number of As in sequence
            but repeats some previously found counts as well
'''

