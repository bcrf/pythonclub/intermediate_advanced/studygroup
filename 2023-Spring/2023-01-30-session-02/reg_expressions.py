# -*- coding: utf-8 -*-

import re

def open_fasta(file_name):
    """Open Fasta sequence files.


    Parameters
    ----------
    file_name : Str
        File name.

    Returns
    -------
    fasta_seq : Str
        Seqeunce data contained in the Fasta file.

    """

    # open file
    seq_file = open(file_name, 'r')

    data = seq_file.read()

    seq_file.close()

    #index of first \n character
    index = data.find('\n')

    # get nucleotide sequence after first \n
    fasta_seq = data[index+1:].replace('\n', '')

    fasta_seq = fasta_seq.upper()

    return fasta_seq




def find_sequence(expression, sequence):
    """Find regular expression in sequence.

    Parameters
    ----------
    expression : Str
        Regular expression.
    sequence : Str
        String where the search is performed.

    Returns
    -------
    None.

    """

    hit_list = re.finditer(expression, sequence)

    print('{:<10s} {:<10s} {:s}'. format('start', 'end', 'match'))

    for hit in hit_list:

        match = hit.group(0)

        start, end = hit.span()

        print('{:<10d} {:<10d} {:s}'. format(start+1, end, match))






#Y =open_fasta('chrY.fna')

s = 'ACCTGAACGAGTGTGAGTAGGCCA'

find_sequence('GT', s)