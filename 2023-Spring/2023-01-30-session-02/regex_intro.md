## Regular expressions

A regular expression, often shortened to "regex," is a sequence of characters that define a *search pattern*. Regex is used in many programming languages to perform pattern matching on strings, *i.e.*, "***find and replace***" type operations.

### Regular expression examples

A regex to match a single digit: `\d`

A regex to match a sequence of digits could be: `\d+`

A regex to match a single alphanumeric character: `\w`

A regex to match a valid email address might be: `\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b`.

A regex to match a US phone number in the format `xxx-xxx-xxxx` might be: `\b\d{3}-\d{3}-\d{4}\b`.

A regex to match all the occurrences of the word "dog" in a sentence could be: `\bdog\b`. `\b` defines the *boundaries* of the word.


## Worked example

Here's a worked example using the phrase "*The quick brown fox jumps over the lazy dog*" and a regular expression that will extract the word "fox" and the word "dog" from the sentence:

- Text: "The quick brown fox jumps over the lazy dog"
- Regular Expression: `\b(fox|dog)\b`

The regular expression `\b(fox|dog)\b` matches the word "fox" or "dog" surrounded by word boundaries `\b`. The regular expression will extract the words "fox" and "dog" from the sentence.

### Using `grep` in Shell

`grep` is used to search for patterns in text files and is commonly used in Unix and Linux operating systems. The expression \b(fox|dog)\b would find all occurrences of either "fox" or "dog" surrounded by word boundaries in a text file.

Here's an example usage of grep with the expression `\b(fox|dog)\b`:

```bash
$ echo "The quick brown fox jumps over the lazy dog" | grep -o "\b(fox|dog)\b"
fox
dog
```

The `-o` option tells `grep` to only print the matching portion of each line, rather than the whole line.

### Using Python

The following python script extracts the words "fox" and "dog" from the sentence "The quick brown fox jumps over the lazy dog" using regular expressions:

```python
import re

text = "The quick brown fox jumps over the lazy dog"
regex = re.compile(r"\b(fox|dog)\b")
matches = regex.findall(text)

print(matches)
```
The output would be a **list**:
```python
['fox', 'dog']
```

In this script, the regular expression `\b(fox|dog)\b` is compiled using the `re.compile` function and stored in the `regex` variable. The `re.findall` function is then used to find all non-overlapping occurrences of the regular expression in the text string, and the result is stored in the matches list. The print function is used to display the matches list, which contains the extracted words "fox" and "dog".


The output `['fox', 'dog']` is a **list of strings** in Python.

In Python, a list is a collection of items that are ordered and *changeable*. Lists are created using square brackets `[]` and items are separated by commas. Lists can contain elements of different data types, including strings like "fox" and "dog". The type of the variable that holds a list is `list`. You can check the type of a variable using the type function in Python, like this:

```python
print(type(['fox', 'dog']))
```
Output:

```python
<class 'list'>
```

## Notes

"**The quick brown fox jumps over the lazy dog**" is an example of a ***pangram*** from the English language. A pangram sentence is a type of sentence that uses every letter of a given alphabet at least once. Pangram sentences are also known as holoalphabetic sentences. 

## Resources

* Regular expressions with Python: https://www.guru99.com/python-regular-expressions-complete-tutorial.html
* Using regular expressions to find text: https://support.unicomsi.com/manuals/intelligence/75/index.html#page/Desktop%20User%20Guides/UsingProfessional.041.039.html
* Searching with Regular Expressions: https://help.relativity.com/RelativityOne/Content/Relativity/Regular_expressions/Searching_with_regular_expressions.htm)
* Syntax and examples: https://www3.ntu.edu.sg/home/ehchua/programming/howto/Regexe.html

### Acknowledgments

This tutorial was written in part with the help of openAI ChatGPT

JYS
