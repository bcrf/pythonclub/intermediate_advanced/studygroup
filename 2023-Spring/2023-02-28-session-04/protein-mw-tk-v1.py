from tkinter import Tk
from tkinter.filedialog import askopenfilename, asksaveasfilename
import os

# create a GUI window
root = Tk()
root.withdraw()

# create input file selection widget
input_file = askopenfilename(
    title="Select input file",
    filetypes=[("Text files", "*.txt"), ("PEP files", "*.pep"), ("All files", "*.*")],
)

# create output file selection widget
output_file = asksaveasfilename(
    title="Save output file as",
    filetypes=[("CSV files", "*.csv"), ("Markdown files", "*.md"), ("All files", "*.*")],
    defaultextension=".csv"
)

# load the protein sequence from the input file
with open(input_file, "r") as f:
    sequence = f.read()

# define a dictionary of amino acids and their molecular weights
aa_weights = {
    "A": 89.09,
    "R": 174.20,
    "N": 132.12,
    "D": 133.10,
    "C": 121.15,
    "E": 147.13,
    "Q": 146.15,
    "G": 75.07,
    "H": 155.16,
    "I": 131.17,
    "L": 131.17,
    "K": 146.19,
    "M": 149.21,
    "F": 165.19,
    "P": 115.13,
    "S": 105.09,
    "T": 119.12,
    "W": 204.23,
    "Y": 181.19,
    "V": 117.15,
}

# calculate the molecular weight of the protein sequence
total_weight = sum([aa_weights.get(aa, 0) for aa in sequence])

# calculate the percentage of each amino acid in the protein sequence
aa_percentages = {aa: (sequence.count(aa) / len(sequence) * 100) for aa in aa_weights}

# format the output table
output_table = "Amino Acid,Count,Percentage\n"
for aa, weight in aa_weights.items():
    count = sequence.count(aa)
    percentage = aa_percentages.get(aa, 0)
    output_table += f"{aa},{count},{percentage:.2f}\n"
output_table += f"Total,{len(sequence)},{100.00:.2f}\n"

# save the output table to a file
with open(output_file, "w") as f:
    f.write(output_table)

# show message that the file has been saved
print(f"Output file saved as {os.path.basename(output_file)}")

# close the GUI window
root.destroy()
