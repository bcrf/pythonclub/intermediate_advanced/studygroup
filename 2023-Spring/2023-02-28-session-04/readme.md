## Graphical User Interface with Python

We looked at the examples in [***Create Graphical User Interfaces with Python***](https://www.dbooks.org/create-graphical-user-interfaces-with-python-1912047918/). ([Jupyter Notebooks](https://github.com/themagpimag/createguis).) The book is a demonstration of using the simple `guizero` package, which is a "wrapper" to a more complex package. `guizero` documentation is at https://lawsie.github.io/

We looked at Chapters 1-3 with code from the book summarized in the Jupyter notebook [`GUI_01.ipynb`](./GUI_01.ipynb)

We tried a few commands taken from https://lawsie.github.io/guizero/usingtk/  and gathered in Jupyter notebook [`GUI_02-tkinter.ipynb`](./GUI_02-tkinter.ipynb)

In `guizero` there is no provision to **open/close a file**. For this we'll add options from `tkinter` (package name: `tk`.)

As a way to use an open and close file command from `tk` we explored a simple Python Script to compute the amino acid composition of a protein contained in a text file. For this the help of *ChatGPT* was enrolled to modify an exisiting script that was studied in the Beginners group: (See [ChatGPT_Protein_Composition.ipynb](https://git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup/-/blob/main/2023-Spring/2023-02-09-Beginners-session-05/ChatGPT_Protein_Composition.ipynb), or Jupyter viewer [ChatGPT_Protein_Composition.ipynb](https://nbviewer.org/urls/git.doit.wisc.edu/bcrf/pythonclub/beginners/studygroup/-/raw/main/2023-Spring/2023-02-09-Beginners-session-05/ChatGPT_Protein_Composition.ipynb).  Code inspired by [number-of-occurences-of-letters-in-a-word](https://unix.stackexchange.com/questions/168379/number-of-occurences-of-letters-in-a-word).)

The first iterations are in files:

* [`composition-protein-v1.py`](./composition-protein-v1.py)
* [`composition-protein-v2.py`](./composition-protein-v2.py)

The supplied files [`insulin.txt`](./insulin.txt) and [`insulin.pep`](./insulin.pep) contain the same text of human insulin sequence.
Version 1 can only "see" the `.txt` files (others are gray) while version 2 can see both.

*Note*: these are not really "fasta" formatted files as it is missing the `>Name` notation.

The output file is written in markdown format.

Subsequent versions 3,4,5,7 were "not functional.

The next requests yield

* [`protein-mw-tk-v1.py`](./protein-mw-tk-v1.py)
* [`protein-mw-tk-v2.py`](./protein-mw-tk-v2.py)

The output now computes molecular weight, as ChatGPT changed the nature of the dictionary. However, the code is useful in illustrating all the steps necessary to create a functional GUI, including selecting and writing output. The code makes use of user-defined functions.

The aa composition from version 2 is in the form *e.g.* for Leucine: `1L: 0.18`, and we can verify that the numbers sum to ` 1 ` (*i.e.* 100%) with a shell (`bash` or `zsh`) script. Assuming that the output file name is `output.txt`:

```
egrep "^[A-Z]:" < output.txt |awk 'END { print s } { s += $2 }' 
```
The output would be `1.01` due to rounding.

(Credit to `awk` command to [sum-a-column-of-numbers](https://stackoverflow.com/questions/3096259/bash-command-to-sum-a-column-of-numbers).)


