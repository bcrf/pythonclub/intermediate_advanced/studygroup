'''
This code uses the tkinter library to create file dialogs for selecting the 
input and output files, and the guizero library to create a simple GUI with 
text boxes and buttons. The analyze_protein function reads the protein 
sequence from the input file selected by the user, analyzes its amino acid 
composition using the same code as before, and saves the result in the 
output file selected by the user.
'''


import os
import tkinter as tk
from tkinter import filedialog
from guizero import App, Text, TextBox, PushButton

amino_acids = {'A': 'Alanine', 'C': 'Cysteine', 'D': 'Aspartic Acid', 'E': 
'Glutamic Acid',
               'F': 'Phenylalanine', 'G': 'Glycine', 'H': 'Histidine', 'I': 
'Isoleucine',
               'K': 'Lysine', 'L': 'Leucine', 'M': 'Methionine', 'N': 
'Asparagine',
               'P': 'Proline', 'Q': 'Glutamine', 'R': 'Arginine', 'S': 
'Serine',
               'T': 'Threonine', 'V': 'Valine', 'W': 'Tryptophan', 'Y': 
'Tyrosine'}

def select_input_file():
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename(filetypes=[("Text files", 
"*.txt")])
    input_file.value = file_path

def select_output_file():
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.asksaveasfilename(defaultextension=".md")
    output_file.value = file_path

app = App(title="Protein Analysis")

Text(app, text="Select input file:")
input_file = TextBox(app, width=50)
PushButton(app, command=select_input_file, text="Browse...")

Text(app, text="Output file name:")
output_file = TextBox(app, width=50)
PushButton(app, command=select_output_file, text="Browse...")

def analyze_protein():
    with open(input_file.value, "r") as f:
        protein_sequence = f.read().strip()

    aa_count = {}
    for aa in amino_acids:
        aa_count[aa] = protein_sequence.count(aa)

    total_aa = sum(aa_count.values())

    with open(output_file.value, "w") as f:
        f.write("# Protein Composition\n\n")
        f.write("Amino Acid | Count | Percentage\n")
        f.write("--- | --- | ---\n")
        for aa, count in aa_count.items():
            percentage = (count / total_aa) * 100
            f.write(f"{amino_acids[aa]} | {count} | {percentage:.2f}%\n")

PushButton(app, command=analyze_protein, text="Analyze Protein")

app.display()
