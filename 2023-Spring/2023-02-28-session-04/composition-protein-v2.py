'''
WARNING:
THIS CODE will not allow you to select the .txt or the .pep file.

It is here to show FAILURE
'''

'''
V1:
This code uses the tkinter library to create file dialogs for selecting the 
input and output files, and the guizero library to create a simple GUI with 
text boxes and buttons. The analyze_protein function reads the protein 
sequence from the input file selected by the user, analyzes its amino acid 
composition using the same code as before, and saves the result in the 
output file selected by the user.
'''

'''
V2: 
This code adds a check for the existence of the input file, and displays an 
error message if it doesn't exist. It also displays a success message with 
the output file name when the analysis is completed and the output file is 
written.
'''

import os
import tkinter as tk
from tkinter import filedialog
from guizero import App, Text, TextBox, PushButton, info

amino_acids = {'A': 'Alanine', 'C': 'Cysteine', 'D': 'Aspartic Acid', 'E': 
'Glutamic Acid',
               'F': 'Phenylalanine', 'G': 'Glycine', 'H': 'Histidine', 'I': 
'Isoleucine',
               'K': 'Lysine', 'L': 'Leucine', 'M': 'Methionine', 'N': 
'Asparagine',
               'P': 'Proline', 'Q': 'Glutamine', 'R': 'Arginine', 'S': 
'Serine',
               'T': 'Threonine', 'V': 'Valine', 'W': 'Tryptophan', 'Y': 
'Tyrosine'}

def select_input_file():
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.askopenfilename(filetypes=[("Text files", 
"*.txt;*.pep")])
    input_file.value = file_path

def select_output_file():
    root = tk.Tk()
    root.withdraw()
    file_path = filedialog.asksaveasfilename(defaultextension=".md")
    output_file.value = file_path

app = App(title="Protein Analysis")

Text(app, text="Select input file:")
input_file = TextBox(app, width=50)
PushButton(app, command=select_input_file, text="Browse...")

Text(app, text="Output file name:")
output_file = TextBox(app, width=50)
PushButton(app, command=select_output_file, text="Browse...")

def analyze_protein():
    input_path = input_file.value
    output_path = output_file.value

    if not input_path or not output_path:
        info("Error", "Please select input and output files")
        return

    if not os.path.isfile(input_path):
        info("Error", f"Input file '{input_path}' does not exist")
        return

    with open(input_path, "r") as f:
        protein_sequence = f.read().strip()

    aa_count = {}
    for aa in amino_acids:
        aa_count[aa] = protein_sequence.count(aa)

    total_aa = sum(aa_count.values())

    with open(output_path, "w") as f:
        f.write("# Protein Composition\n\n")
        f.write("Amino Acid | Count | Percentage\n")
        f.write("--- | --- | ---\n")
        for aa, count in aa_count.items():
            percentage = (count / total_aa) * 100
            f.write(f"{amino_acids[aa]} | {count} | {percentage:.2f}%\n")

    info("Success", f"Output written to '{output_path}'")

PushButton(app, command=analyze_protein, text="Analyze Protein")

app.display()
