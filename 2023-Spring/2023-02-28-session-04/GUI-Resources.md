## GUI Resources

Here are some resources I read or watched during preparation:

* GuiZero [documentation](https://lawsie.github.io/guizero/start/)
* GUI zero (compared to tkinter) https://pythonprogramming.altervista.org/gui-zero/
* Using GuiZero: [How to turn your program into a graphical application](https://www.futurelearn.com/info/courses/block-to-text-based-programming/0/steps/39499) (video and article.)
* Python GUI open a file (*tkinter* `filedialog`) - [YouTube](https://youtu.be/q8WDvrjPt0M)
* More advanced:[Python GUI Programming With Tkinter](https://realpython.com/python-gui-tkinter/)

There are many other resources online. Here are a few YouTube options I found:

From [Codemy.com](https://codemy.com) on YouTube ([Channel](https://www.youtube.com/@Codemycom).)

* [Tkinter Course](https://youtu.be/YXPyB4XeYLA) - **5h37min** - Create Graphic User Interfaces in Python Tutorial (Nov 2019 - formely 
freeCodeCamp.org)
* [Modern GUI Design With Tkinter](https://youtu.be/JDU-ycsxvqM) **8min 51** - Python Tkinter GUI Tutorial 219 (May 2022)
