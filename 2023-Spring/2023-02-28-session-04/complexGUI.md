## A complex GUI interface

This Stack Overflow question [How can I set this GUI in the way I want using Guizero?](https://stackoverflow.com/questions/71902553/how-can-i-set-this-gui-in-the-way-i-want-using-guizero) was answered with lots of details and the code.

I add the code here as file [complex-form.py](./complex-form.py) as a simple copy/paste causes indentation error which is otherwise easily corrected by removing all the blank spaces at the beginning of each line (easily one with `vi`/`vim` editor.)

I tested file [complex-form.py](./complex-form.py) on my Mac and it worked.
