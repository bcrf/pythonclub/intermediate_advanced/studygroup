''' 
This code creates three Box widgets for selecting the input file,
output file, and running the analysis, and adds PushButton widgets to
each box to handle the file selection and analysis. The selected file
paths are stored in Text widgets and passed to the analyse_protein
function when the analysis button is clicked. The results are written to
the selected output file and a message is displayed to 
'''

from Bio.SeqUtils.ProtParam import ProteinAnalysis
from tkinter import Tk
from tkinter.filedialog import askopenfilename, asksaveasfilename
from guizero import App, Box, Text, PushButton

def select_input_file():
    # open file dialog to select input file
    root = Tk()
    root.withdraw()
    filename = askopenfilename(title="Select Input File", filetypes=[("Text Files", "*.txt"), ("PEP Files", "*.pep"), ("All Files", "*.*")])
    input_file.value = filename

def select_output_file():
    # open file dialog to select output file
    root = Tk()
    root.withdraw()
    filename = asksaveasfilename(title="Select Output File", defaultextension=".txt", filetypes=[("Text Files", "*.txt"), ("Markdown Files", "*.md"), ("All Files", "*.*")])
    output_file.value = filename

def analyse_protein():
    # get input and output file paths
    input_path = input_file.value
    output_path = output_file.value
    
    # check if input and output files are selected
    if not input_path or not output_path:
        message.value = "Please select input and output files."
        return
    
    # calculate molecular weight
    with open(input_path) as f:
        sequence = f.read().strip()
    analysed_seq = ProteinAnalysis(sequence)
    mw = analysed_seq.molecular_weight()
    
    # write output to file
    with open(output_path, "w") as f:
        f.write(f"Protein Sequence: {sequence}\n")
        f.write(f"Molecular Weight: {mw:.2f} Da\n")
        f.write(f"Composition (%):\n")
        for aa, perc in analysed_seq.get_amino_acids_percent().items():
            f.write(f"{aa}: {perc:.2f}\n")
    
    # update message
    message.value = f"Analysis results saved to {output_path}."

# create app
app = App(title="Protein Analysis")

# create widgets
input_box = Box(app, align="top", border=True, width="fill")
Text(input_box, text="Select Input File:")
input_file = Text(input_box, text="(no file selected)", width=50)
PushButton(input_box, text="Select", command=select_input_file)

output_box = Box(app, align="top", border=True, width="fill")
Text(output_box, text="Select Output File:")
output_file = Text(output_box, text="(no file selected)", width=50)
PushButton(output_box, text="Select", command=select_output_file)

run_box = Box(app, align="top", border=True, width="fill")
PushButton(run_box, text="Run Analysis", command=analyse_protein)

message = Text(app, text="")

# run app
app.display()
