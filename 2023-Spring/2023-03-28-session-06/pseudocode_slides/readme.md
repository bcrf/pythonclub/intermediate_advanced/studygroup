## Pseudocode slides

All slides are derived from the `pseudocode_slides.Rmd` when rendered by `knitr` package within RStudio.

File names were altered to add the format within the name *e.g.* adding `slidy` or `beamer`.

Powerpoint slides were manually adjusted for font size.
