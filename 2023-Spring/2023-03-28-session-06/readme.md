## Topic: pseudocode

Attendance: 1

### Pseudocode and ChatGPT

Some pseudocode was test from the book ***"Impractical Python Projects: Playful Programming Activities to Make You Smarter"*** by Lee Vaughan. [Code](https://github.com/rlvaugh/Impractical_Python_Projects)

## Extended topic: Python within RStudio, making slide

The pseudocode topic was interesting, but we also took this opportunity to demonstrate how to use Python within RStudio and create slides (HTML/PowerPoint/PDF) and regular documents from the SAME `.Rmd` document with imbeded Python code.

The code and all resulting files for pseudocode slides and Python testing in R are located in separate directories for better clarity.

More information on:

HTML format:

* Slidy: https://garrettgman.github.io/rmarkdown/slidy_presentation_format.html
* ioslides: https://bookdown.org/yihui/rmarkdown/ioslides-presentation.html

Other formats can also be looked up at https://bookdown.org/yihui/rmarkdown/
