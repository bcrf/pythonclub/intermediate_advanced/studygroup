# -*- coding: utf-8 -*-
import json


def open_annotation(annotation_file):
    """Open anotation file.

    Parameters
    ----------
    annotation_file : string
        Annotation file name.

    Returns
    -------
    genes : List
        List containing genes.

    """

    # open file
    file = open(annotation_file, 'r')

    data = json.load(file)

    file.close()

    # List of genes
    genes = []

    for geneID in data:

        # get gene data
        gene_dictionary = data[geneID]

        seq_range = gene_dictionary['seq_range']
        seq_type = 'gene'
        orientation = gene_dictionary['orientation']

        gene = Gene(seq_type, seq_range, orientation, geneID)

        genes.append(gene)

    return genes


def open_repeats(repeat_file, orientation):
    """Open repeat data file.

    Parameters
    ----------
    repeat_file : String
        Repeats data file.
    orientation : String
        Repeat sequence orientation, it can be '+' or '-'.

    Returns
    -------
    repeats : List
        List of repeats sequences.

    """
    # Open data file
    file = open(repeat_file, 'r')

    lines = file.readlines()

    file.close()

    # repeats list
    repeats = []

    for index in range(1, len(lines)):

        line = lines[index].split()

        start = int(line[0])

        end = int(line[1])

        repeat = Sequence('repeat', (start, end), orientation)

        repeats.append(repeat)

    return repeats
