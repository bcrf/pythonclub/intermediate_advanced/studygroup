# Session 03 - Object Oriented programing - based on Chapter 7

Thank you to Cristian and Kasia  for volunteering. 

Kasia presented materials from Chapter 7. This material is contained within Jupyter notebook [`20230214_ObjectOrientedProgramming_Intro.ipynb`](https://git.doit.wisc.edu/bcrf/pythonclub/intermediate_advanced/studygroup/-/blob/main/2023-Spring/2023-02-14-session-03/20230214_ObjectOrientedProgramming_Intro.ipynb)

Cristian showed an implementation of the use of `class` and user-defined `def` functions to identify where a list of previously recorded repeat GT patterns match on a chromosome sequence. The file [`Analysis.py`](https://git.doit.wisc.edu/bcrf/pythonclub/intermediate_advanced/studygroup/-/blob/main/2023-Spring/2023-02-14-session-03/Analysis.py) was finished in class with added classes and functions. The final Jupyter notebook is [`Analysis.ipynb`](https://git.doit.wisc.edu/bcrf/pythonclub/intermediate_advanced/studygroup/-/blob/main/2023-Spring/2023-02-14-session-03/Analysis.ipynb).

